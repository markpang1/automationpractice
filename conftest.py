import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .lib.login_page import LoginPage
from .lib.home_page import HomePage


# class Driver:
# 	def __init__(self):
# 		self.driver = webdriver.Chrome()
# 		#self.driver.get("https://phptravels.com/demo")
# 		self.driver.get("http://automationpractice.com/index.php")

@pytest.fixture(scope='class')
def init_driver(request):
    options = Options()
    options.headless = True
    driver = webdriver.Chrome(options=options)
    driver.get("http://automationpractice.com/index.php")
    request.cls.browser = driver
    yield
    # Any teardown code for that fixture is placed after the yield.
    driver.quit()

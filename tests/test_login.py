import pytest
from ..lib.home_page import HomePage
from ..lib.login_page import LoginPage
from .test_base import TestBase


class TestLogin(TestBase):

    # https://docs.pytest.org/en/6.2.x/fixture.html#safe-fixture-structure

    @pytest.fixture(scope='class')
    def login_page(self):
        home_page = HomePage(self.browser)
        home_page.click_login()
        return LoginPage(self.browser)

    def test_login_bad_email(self, login_page):
        login_page.enter_email("markpang1@gmail.com")
        login_page.enter_password("badpassword")
        login_page.click_sign_in()
        assert "Authentication failed." in login_page.get_login_error_message()

    def test_create_account_bad_email(self, login_page):
        login_page.enter_email_create_account("xyz")
        login_page.click_create_account()
        assert "Invalid email address." in login_page.get_create_account_error_message()

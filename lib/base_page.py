from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest


class BasePage:
    def __init__(self, browser):
        self.browser = browser

    def get_element_by_css(self, selector):
        WebDriverWait(self.browser, 30).until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
        elem = self.browser.find_element(By.CSS_SELECTOR, selector)
        return elem
    # NoSuchElementException:

from .base_page import BasePage


class HomePage(BasePage):
    def click_login(self):
        self.get_element_by_css(".login").click()

from .base_page import BasePage


class LoginPage(BasePage):
    def enter_email(self, email):
        elem = self.get_element_by_css("#email")
        elem.send_keys(email)

    def enter_password(self, password):
        elem = self.get_element_by_css("#passwd")
        elem.send_keys(password)

    def click_sign_in(self):
        self.get_element_by_css("#SubmitLogin").click()

    def get_login_error_message(self):
        return self.get_element_by_css(".alert").text

    def enter_email_create_account(self, email):
        elem = self.get_element_by_css("#email_create")
        elem.send_keys(email)

    def click_create_account(self):
        self.get_element_by_css("#SubmitCreate").click()

    def get_create_account_error_message(self):
        return self.get_element_by_css("#create_account_error").text
